﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;

namespace netdatabase
{
    class Program
    {
        private static string connectionString = "./sample.lite.db";

        static void Main(string[] args)
        {
            // set up
            var settings = new LiteDbStorage.Settings {connectionString = connectionString};
            var dataSource = new DataSourceBuilder().With<LiteDbStorage>().Using(settings).Build();

            using(var context = dataSource.GetContext())
            {
                Table<FakeModel> table = context.Table<FakeModel>();

                table.Write(new FakeModel{ ID = 1, Name = "Henry Nitz"});
                table.Write(new FakeModel{ ID = 2, Name = "Joe Doe"});
                table.Write(new FakeModel{ ID = 3, Name = "Jane Wane"});
                table.Write(new FakeModel{ ID = 4, Name = "Zach Nack"});

                var all_records = table.Select();
                Console.WriteLine($"all_records, count: {all_records.Count()}");

                var filtered = table.Select(null, x => x.Where(x => x.ID > 2));
                Console.WriteLine($"filtered, count: {filtered.Count()}");

                var view = table.Select("View_Nitz");
                Console.WriteLine($"view, count: {view.Count()}");
            }
        }
    }
}
