using System;
using System.Reflection;
using Autofac;

class DataSourceBuilder
{
    private readonly ContainerBuilder builder;

    public DataSourceBuilder()
    {
        var asm = Assembly.GetExecutingAssembly();

        builder = new Autofac.ContainerBuilder();
        builder.RegisterAssemblyTypes(asm).AsClosedTypesOf(typeof(ITrigger<>));
        builder.RegisterAssemblyTypes(asm).AsClosedTypesOf(typeof(IView<>));
        builder.RegisterGeneric(typeof(Table<>));
    }

    public DataSourceBuilder With<TProvider>() where TProvider : IStorageProvider
    {
        builder.RegisterType<TProvider>().As<IStorageProvider>().SingleInstance();
        return this;
    }

    public DataSourceBuilder Using(object settings)
    {
        builder.RegisterInstance(settings).AsSelf();
        return this;
    }

    public DataSource Build()
    {
        return new DataSource(builder.Build());
    }
}

class DataSource
{
    private IContainer container;

    public DataSource(IContainer container)
    {
        this.container = container;
    }

    public DataSourceContext GetContext()
    {
        return new DataSourceContext(container.BeginLifetimeScope());
    }
}

class DataSourceContext : IDisposable
{
    private readonly ILifetimeScope scope;

    public DataSourceContext(ILifetimeScope scope)
    {
        this.scope = scope;
    }

    public void Dispose()
    {
        scope.Dispose();
    }

    public Table<TModel> Table<TModel>()
    {
        return scope.Resolve<Table<TModel>>();
    }
}
