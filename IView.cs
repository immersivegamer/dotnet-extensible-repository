using System.Linq;

internal interface IView<T>
{
    string id { get; }

    IQueryable<T> Filter(IQueryable<T> arg);
}