internal interface ITrigger<T>
{
    void AfterRead(T x);
    void BeforeWrite(T record);
    void AfterWrite(T record);
    void BeforeDelete(T record);
    void AfterDelete(T record);
}

abstract class BaseTrigger<Y> : ITrigger<Y>
{
    public virtual void AfterDelete(Y record) { }
    public virtual void AfterRead(Y x) { }
    public virtual void AfterWrite(Y record) { }
    public virtual void BeforeDelete(Y record) { }
    public virtual void BeforeWrite(Y record) { }
}