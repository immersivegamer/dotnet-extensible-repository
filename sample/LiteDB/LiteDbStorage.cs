using System;
using System.Linq;
using System.Linq.Expressions;
using LiteDB;

class LiteDbStorage : IStorageProvider, IDisposable
{
    private readonly LiteDatabase database;

    public LiteDbStorage(LiteDbStorage.Settings settings)
    {
        this.database = new LiteDatabase(settings.connectionString);
    }

    private ILiteCollection<T> GetLiteCollectionByType<T>()
    {
        return database.GetCollection<T>(typeof(T).Name);
    }

    public IQueryable<T> Get<T>()
    {
        return GetLiteCollectionByType<T>().FindAll().AsQueryable();
    }

    public void Index<T>(Expression<Func<T, object>> propertySelector, bool unique = false)
    {
        GetLiteCollectionByType<T>().EnsureIndex(propertySelector,unique);
    }

    public void Update<T>(T record)
    {
        GetLiteCollectionByType<T>().Upsert(record);
    }

    public void Delete<T>(T record)
    {
        throw new NotImplementedException();
    }

    public void Insert<T>(T record)
    {
        GetLiteCollectionByType<T>().Insert(record);
    }

    public void Dispose()
    {
        database.Dispose();
    }

    internal class Settings
    {
        public string connectionString { get; set; }
    }
}