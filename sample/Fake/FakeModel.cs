class FakeModel {
    public int ID { get; set; }
    public string Name { get; set; }
    public string Data { get; set; }
}