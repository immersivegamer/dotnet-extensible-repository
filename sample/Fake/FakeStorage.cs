using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

class FakeStorage : IStorageProvider
{
    public void Delete<T>(T record)
    {
        throw new NotImplementedException();
    }

    public IQueryable<T> Get<T>()
    {
        if(typeof(T) == typeof(FakeModel)) {
            return new List<FakeModel>() {
                new FakeModel { ID = 1, Name="Henry Nitz", Data = ""},
                new FakeModel { ID = 2, Name="Joe Doe", Data = ""},
                new FakeModel { ID = 3, Name="Jane Wane", Data = ""},
                new FakeModel { ID = 4, Name="Zach Nack", Data = ""}
            }.Cast<T>().AsQueryable();
        }
        else
        {
            throw new NotImplementedException();
        }
    }

    public void Index<T>(Expression<Func<T, object>> propertySelector)
    {
        throw new NotImplementedException();
    }

    public void Index<T>(Expression<Func<T, object>> propertySelector, bool unique = false)
    {
        throw new NotImplementedException();
    }

    public void Insert<T>(T record)
    {
        throw new NotImplementedException();
    }

    public void Update<T>(T record)
    {
        throw new NotImplementedException();
    }
}