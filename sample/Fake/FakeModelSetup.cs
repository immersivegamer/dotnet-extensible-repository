using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;

class FakeModelSetup {
    public void Run()
    {
        var datasource = new DataSourceBuilder().With<FakeStorage>().Build();

        using(var context = datasource.GetContext())
        {
            var table = context.Table<FakeModel>();

            var all_records = table.Select();
            var filtered = table.Select(null, x => x.Where(x => x.ID > 2));
            var view = table.Select("View_Nitz");
            
            Console.WriteLine($"all_records, count: {all_records.Count()}");
            Console.WriteLine($"filtered, count: {filtered.Count()}");
            Console.WriteLine($"view, count: {view.Count()}");
        }
    }
}

class SampleView : IView<FakeModel>
{
    private readonly IStorageProvider storage;

    public SampleView(IStorageProvider storage)
    {
        this.storage = storage;
    }

    public string id => "View_Nitz";

    public IQueryable<FakeModel> Filter(IQueryable<FakeModel> arg)
    {
        return arg.Where(x => x.Name.Contains("Nitz"));
    }
}

class SampleTrigger : ITrigger<FakeModel>
{
    public void AfterDelete(FakeModel record)
    {
    }

    public void AfterRead(FakeModel record)
    {
        System.Console.WriteLine($"Read record, ID: {record.ID}, Name: {record.Name}");
    }

    public void AfterWrite(FakeModel record)
    {
        System.Console.WriteLine($"Wrote record, ID: {record.ID}, Name: {record.Name}");
    }

    public void BeforeDelete(FakeModel record)
    {
    }

    public void BeforeWrite(FakeModel record)
    {
        record.Data = DateTime.Now.ToString();
    }
}