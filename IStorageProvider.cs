using System;
using System.Linq;
using System.Linq.Expressions;

internal interface IStorageProvider
{
    void Update<T>(T record);
    IQueryable<T> Get<T>();
    void Delete<T>(T record);
    void Insert<T>(T record);
    void Index<T>(Expression<Func<T, object>> propertySelector, bool unique = false);
}