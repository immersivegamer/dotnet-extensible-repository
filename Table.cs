using System;
using System.Collections.Generic;
using System.Linq;

class Table<T>
{
    private readonly IStorageProvider storage;
    private readonly IEnumerable<ITrigger<T>> triggers;
    private readonly IEnumerable<IView<T>> views;
    private readonly Type _type;

    // fetch data
    public Table(
        IStorageProvider storage,
        IEnumerable<ITrigger<T>> triggers,
        IEnumerable<IView<T>> views
    )
    {
        this.storage = storage;
        this.triggers = triggers;
        this.views = views;

        _type = typeof(T);
    }

    public IEnumerable<T> Select(string view_id = null, Func<IQueryable<T>, IQueryable<T>> query = null)
    {
        IQueryable<T> data = storage.Get<T>();
        
        if(view_id != null)
        {
            var view = views.First(x => x.id == view_id);
            data = view.Filter(data);
        }

        if(query != null)
        {
            data = query(data);
        }

        IEnumerable<T> result = data.AsEnumerable().Select(x => {
            foreach (var trigger in triggers)
            {
                trigger.AfterRead(x);
            }
            return x;
        });

        return result;
    }

    public void Write(T record)
    {
        foreach (var trigger in triggers)
        {
            trigger.BeforeWrite(record);
        }

        storage.Update(record);

        foreach (var trigger in triggers)
        {
            trigger.AfterWrite(record);
        }
    }

    public void Delete(T record)
    {
        foreach (var trigger in triggers)
        {
            trigger.BeforeDelete(record);
        }

        storage.Delete(record);

        foreach (var trigger in triggers)
        {
            trigger.AfterDelete(record);
        }
    }
}